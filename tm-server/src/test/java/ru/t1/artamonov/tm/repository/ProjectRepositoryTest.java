//package ru.t1.artamonov.tm.repository;
//
//import lombok.SneakyThrows;
//import org.apache.ibatis.session.SqlSession;
//import org.jetbrains.annotations.NotNull;
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.experimental.categories.Category;
//import ru.t1.artamonov.tm.api.repository.IProjectRepository;
//import ru.t1.artamonov.tm.api.service.IConnectionService;
//import ru.t1.artamonov.tm.marker.UnitCategory;
//import ru.t1.artamonov.tm.dto.model.Project;
//import ru.t1.artamonov.tm.service.ConnectionService;
//import ru.t1.artamonov.tm.service.PropertyService;
//
//import static ru.t1.artamonov.tm.constant.ProjectTestData.*;
//import static ru.t1.artamonov.tm.constant.UserTestData.USER1;
//import static ru.t1.artamonov.tm.constant.UserTestData.USER2;
//
//@Category(UnitCategory.class)
//public final class ProjectRepositoryTest {
//
//    @NotNull
//    private final PropertyService propertyService = new PropertyService();
//
//    @NotNull
//    private final IConnectionService connectionService = new ConnectionService(propertyService);
//
//    @NotNull
//    private final SqlSession connection = connectionService.getSqlConnection();
//
//    @NotNull
//    private final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
//
//    @After
//    @SneakyThrows
//    public void dropConnection() {
//        connection.rollback();
//        connection.close();
//    }
//
//    @Before
//    public void init() {
//        projectRepository.clearAll();
//    }
//
//    @Test
//    public void add() {
//        Assert.assertTrue(projectRepository.findAll().isEmpty());
//        projectRepository.add(USER1_PROJECT1);
//        projectRepository.findOneById(USER1_PROJECT1.getId());
//        Assert.assertEquals(USER1_PROJECT1.getId(), projectRepository.findAll().get(0).getId());
//    }
//
//    @Test
//    public void clearByUserId() {
//        Assert.assertTrue(projectRepository.findAll().isEmpty());
//        for (@NotNull Project project : USER1_PROJECT_LIST) {
//            projectRepository.add(project);
//        }
//        Assert.assertEquals(USER1_PROJECT_LIST.size(), projectRepository.findAll().size());
//        projectRepository.clear(USER2.getId());
//        Assert.assertFalse(projectRepository.findAll().isEmpty());
//        projectRepository.clear(USER1.getId());
//        Assert.assertTrue(projectRepository.findAll().isEmpty());
//        projectRepository.add(USER2_PROJECT1);
//        projectRepository.clear(USER1.getId());
//        Assert.assertFalse(projectRepository.findAll().isEmpty());
//    }
//
//    @Test
//    public void findAllByUserId() {
//        Assert.assertTrue(projectRepository.findAll().isEmpty());
//        for (@NotNull Project project : PROJECT_LIST) {
//            projectRepository.add(project);
//        }
//        Assert.assertEquals(USER1_PROJECT_LIST.size(), projectRepository.findAllUserId(USER1.getId()).size());
//    }
//
//    @Test
//    public void findOneByIdByUserId() {
//        Assert.assertTrue(projectRepository.findAll().isEmpty());
//        projectRepository.add(USER1_PROJECT1);
//        projectRepository.add(USER2_PROJECT1);
//        Assert.assertEquals(USER1_PROJECT1.getId(), projectRepository.findOneByIdUserId(USER1.getId(), USER1_PROJECT1.getId()).getId());
//    }
//
//    @Test
//    public void removeByIdByUserId() {
//        Assert.assertTrue(projectRepository.findAll().isEmpty());
//        projectRepository.add(USER1_PROJECT1);
//        projectRepository.add(USER2_PROJECT1);
//        projectRepository.removeByIdUserId(USER1.getId(), USER1_PROJECT1.getId());
//        Assert.assertNull(projectRepository.findOneByIdUserId(USER1.getId(), USER1_PROJECT1.getId()));
//
//    }
//
//    @Test
//    public void existsByIdByUserId() {
//        Assert.assertTrue(projectRepository.findAll().isEmpty());
//        projectRepository.add(USER1_PROJECT1);
//        Assert.assertTrue(projectRepository.existsById(USER1_PROJECT1.getId()));
//        Assert.assertFalse(projectRepository.existsById(USER2_PROJECT1.getId()));
//    }
//
//}
