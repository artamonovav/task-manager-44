package ru.t1.artamonov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.enumerated.Sort;
import ru.t1.artamonov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void clear(@Nullable String userId);

    @NotNull
    Boolean existsByIdUserId(@Nullable String userId, @Nullable String id);

    @Nullable
    List<M> findAllUserId( @Nullable String userId);

    @Nullable
    List<M> findAllUserId(@NotNull String userId, @NotNull Sort sort);

    @Nullable
    List<M> findAllUserId(@NotNull String userId, @NotNull Comparator comparator);

    @Nullable
    M findOneByIdUserId(@Nullable String userId, @Nullable String id);

    long getSizeUserId(@Nullable String userId);

    void removeUserId(@Nullable String userId, @Nullable M model);

    void removeByIdUserId(@Nullable String userId, @Nullable String id);
    
}
