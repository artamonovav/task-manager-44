package ru.t1.artamonov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.repository.model.ISessionRepository;
import ru.t1.artamonov.tm.model.Session;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionRepository implements ISessionRepository {

    @NotNull
    private final EntityManager entityManager;

    public SessionRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final Session session) {
        entityManager.persist(session);
    }

    @Override
    public void update(@NotNull final Session session) {
        entityManager.merge(session);
    }

    @Override
    public void remove(@NotNull final Session session) {
        entityManager.remove(session);
    }

    @Override
    public void removeByUserId(@NotNull final String userId) {
        if(userId.isEmpty()) return;
        @Nullable List<Session> sessions = findAll(userId);
        if (sessions == null) return;
        for (@NotNull Session session : sessions ) {
            entityManager.remove(session);
        }
    }

    @Nullable
    @Override
    public List<Session> findAll() {
        @NotNull final String jpql = "SELECT m FROM Session m";
        return entityManager.createQuery(jpql, Session.class).getResultList();
    }

    @Nullable
    public List<Session> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM Session m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull final String id) {
        return entityManager.find(Session.class, id);
    }

    @Nullable
    @Override
    public Session findOneByIdUserId(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM Session m WHERE m.id = :id AND m.user.id = :userId";
        return entityManager.createQuery(jpql, Session.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

}
