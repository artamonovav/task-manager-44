package ru.t1.artamonov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionDTORepository {

    void add(@NotNull SessionDTO session);

    void update(@NotNull SessionDTO session);

    void remove(@NotNull SessionDTO session);

    void removeByUserId(@NotNull String userId);

    @Nullable
    List<SessionDTO> findAll();

    @Nullable
    SessionDTO findOneById(@NotNull String id);

    @Nullable
    SessionDTO findOneByIdUserId(@NotNull String userId, @NotNull String id);

}
