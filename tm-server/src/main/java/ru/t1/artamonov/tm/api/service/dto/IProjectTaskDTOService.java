package ru.t1.artamonov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.artamonov.tm.api.repository.dto.ITaskDTORepository;

import javax.persistence.EntityManager;

public interface IProjectTaskDTOService {

    @NotNull
    ITaskDTORepository getTaskRepository(@NotNull EntityManager entityManager);

    @NotNull
    IProjectDTORepository getProjectRepository(@NotNull EntityManager entityManager);

    void bindTaskToProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId);

    void removeProjectById(@Nullable final String userId, @Nullable final String projectId);

    void unbindTaskFromProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId);

}
