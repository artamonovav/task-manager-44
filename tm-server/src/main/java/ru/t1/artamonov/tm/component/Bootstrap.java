package ru.t1.artamonov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.artamonov.tm.api.endpoint.*;
import ru.t1.artamonov.tm.api.service.*;
import ru.t1.artamonov.tm.api.service.dto.*;
import ru.t1.artamonov.tm.endpoint.*;
import ru.t1.artamonov.tm.service.*;
import ru.t1.artamonov.tm.service.dto.*;
import ru.t1.artamonov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @Getter
    @NotNull
    private final IProjectTaskDTOService projectTaskService = new ProjectTaskDTOService(connectionService);

    @Getter
    @NotNull
    private final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @Getter
    @NotNull
    private final IUserDTOService userService = new UserDTOService(connectionService, propertyService);

    @Getter
    @NotNull
    private final ISessionDTOService sessionService = new SessionDTOService(connectionService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService, sessionService);

    @NotNull
    private final Backup backup = new Backup(this);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @Getter
    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @Getter
    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @Getter
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @Getter
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @Getter
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @Getter
    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER SERVER STOPPED **");
        backup.stop();
    }

    public void run() {
        initPID();
        loggerService.info("** TASK-MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
    }

}
