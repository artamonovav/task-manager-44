package ru.t1.artamonov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.artamonov.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionDTORepository implements ISessionDTORepository {

    @NotNull
    private final EntityManager entityManager;

    public SessionDTORepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final SessionDTO session) {
        entityManager.persist(session);
    }

    @Override
    public void update(@NotNull final SessionDTO session) {
        entityManager.merge(session);
    }

    @Override
    public void remove(@NotNull final SessionDTO session) {
        entityManager.remove(session);
    }

    @Override
    public void removeByUserId(@NotNull final String userId) {
        if(userId.isEmpty()) return;
        @NotNull String jpql = "DELETE FROM SessionDTO m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll() {
        @NotNull final String jpql = "SELECT m FROM SessionDTO m";
        return entityManager.createQuery(jpql, SessionDTO.class).getResultList();
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@NotNull final String id) {
        return entityManager.find(SessionDTO.class, id);
    }

    @Nullable
    @Override
    public SessionDTO findOneByIdUserId(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM SessionDTO m WHERE m.id = :id AND m.userId = :userId";
        return entityManager.createQuery(jpql, SessionDTO.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

}
