package ru.t1.artamonov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.artamonov.tm.enumerated.Sort;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedDTORepository<M extends AbstractUserOwnedModelDTO> extends IDTORepository<M> {

    void clear(@Nullable String userId);

    @NotNull
    Boolean existsByIdUserId(@Nullable String userId, @Nullable String id);

    @Nullable
    List<M> findAllUserId(@Nullable String userId);

    @Nullable
    List<M> findAllUserId(@Nullable String userId, @NotNull Comparator comparator);

    @Nullable
    List<M> findAllUserId(@Nullable String userId, @NotNull Sort sort);

    @Nullable
    M findOneByIdUserId(@Nullable String userId, @Nullable String id);

    long getSizeUserId(@Nullable String userId);

    void removeUserId(@Nullable String userId, @Nullable M model);

    void removeByIdUserId(@Nullable String userId, @Nullable String id);
    
}
