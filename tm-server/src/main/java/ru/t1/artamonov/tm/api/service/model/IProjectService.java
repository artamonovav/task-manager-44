package ru.t1.artamonov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.repository.model.IProjectRepository;
import ru.t1.artamonov.tm.enumerated.Sort;
import ru.t1.artamonov.tm.enumerated.Status;
import ru.t1.artamonov.tm.model.Project;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    @NotNull
    IProjectRepository getRepository(@NotNull EntityManager entityManager);

    @NotNull
    Project add(@Nullable Project model);

    @NotNull
    Project add(@Nullable String userId, @Nullable Project model);

    @NotNull
    Collection<Project> add(@NotNull Collection<Project> models);

    @NotNull
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    void clear();

    void clear(@Nullable String userId);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    boolean existsById(@Nullable String id);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<Project> findAll();

    @Nullable
    List<Project> findAll(@Nullable String userId);

    @Nullable
    List<Project> findAll(@Nullable Comparator<Project> comparator);

    @Nullable
    List<Project> findAll(@Nullable String userId, @Nullable Comparator<Project> comparator);

    @Nullable
    List<Project> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable
    Project findOneById(@Nullable String id);

    @Nullable
    Project findOneById(@Nullable String userId, @Nullable String id);

    long getSize();

    long getSize(@Nullable String userId);

    @NotNull
    Project remove(@Nullable Project model);

    @NotNull
    Project remove(@Nullable String userId, @Nullable Project model);

    void removeAll(@Nullable Collection<Project> collection);

    @NotNull
    Project removeById(@Nullable String id);

    @NotNull
    Project removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    Collection<Project> set(@NotNull Collection<Project> models);

    @NotNull
    Project update(@NotNull Project model);

    @NotNull
    Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

}
